package io.buchin.notenote.entitys

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

data class NoteGsonEnvelope(
    val noteList: List<Note>
)

@Parcelize
data class Note(
    val id: String = UUID.randomUUID().toString(),
    val title: String = "",
    val noteContent: List<NoteEntry> = emptyList(),
    val backgroundEnable: Boolean = false
) : Parcelable

@Parcelize
data class NoteEntry(
    val id: String = UUID.randomUUID().toString(),
    val content: String = "",
    val isDone: Boolean = false
) : Parcelable
