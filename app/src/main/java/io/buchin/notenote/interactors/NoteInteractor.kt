package io.buchin.notenote.interactors

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v4.app.NotificationCompat
import android.widget.RemoteViews
import com.google.gson.Gson
import io.buchin.notenote.R
import io.buchin.notenote.entitys.Note
import io.buchin.notenote.entitys.NoteGsonEnvelope
import io.buchin.notenote.features.main.MainActivity


private const val SPNotesKey = "SPNotesKey"

class NoteInteractor(
    private val context: Context,
    private val sharedPreferences: SharedPreferences
) {
  fun getNotes() = Gson().fromJson(sharedPreferences.getString(SPNotesKey, null), NoteGsonEnvelope::class.java)
      ?.noteList
      ?: emptyList()

  fun saveNotes(notes: List<Note>) {
    sharedPreferences.edit()
        .putString(SPNotesKey, Gson().toJson(NoteGsonEnvelope(notes)))
        .apply()
  }

  fun saveNote(note: Note) {
    if (note.backgroundEnable) {
      showNoteNotification(note)
    }

    val notes = Gson().fromJson(sharedPreferences.getString(SPNotesKey, null), NoteGsonEnvelope::class.java)
        ?.noteList
        ?.toMutableList()
        ?.let {
          val findNote = it.find { it.id == note.id }
          if (findNote != null) {
            it[it.indexOf(findNote)] = note
            it
          } else {
            it.add(note)
            it
          }
        }
        ?.toList()
        ?: listOf(note)

    saveNotes(notes)
  }

  fun removeNote(note: Note) {
    val notes = getNotes()
        .toMutableList().let {
          it.remove(note)
          it
        }
        .toList()

    saveNotes(notes)
  }

  fun cancelNoteNotification(note: Note) {
    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
      cancel(note.id.hashCode())
    }
  }

  fun showNoteNotification(note: Note) {
    val content = RemoteViews(context.packageName, R.layout.notification).apply {
      setTextViewText(R.id.textview_notification_note_title, note.title)

      var noteContent = ""
      note.noteContent.forEach {
        noteContent += if (it.isDone) {
          //✓ ✅ ☑
          " ✓ ${it.content}\n"
        } else {
          " - ${it.content}\n"
        }
      }

      setTextViewText(R.id.textview_notification_note_content, noteContent)
    }

    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
      val notification = NotificationCompat.Builder(context, "1")
          .setSmallIcon(R.mipmap.ic_launcher)
          .setOngoing(true)
          .setCustomContentView(content)
          .setCustomBigContentView(content)
          .setStyle(NotificationCompat.DecoratedCustomViewStyle())
          .apply {
            val toNotePendingIntent = Intent(context, MainActivity::class.java)
                .apply {
                  putExtra("note", note)
                }
                .let {
                  PendingIntent.getActivity(context, 0, it, PendingIntent.FLAG_UPDATE_CURRENT)
                }
            addAction(android.R.drawable.arrow_down_float, "Перейти к заметке", toNotePendingIntent)
          }
          .build()

      notify(note.id.hashCode(), notification)
    }
  }
}
