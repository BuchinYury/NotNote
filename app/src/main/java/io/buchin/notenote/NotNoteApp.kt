package io.buchin.notenote

import android.app.Application
import io.buchin.notenote.common.AppModule

class NotNoteApp : Application() {
  val appModule by lazy {
    AppModule(this)
  }
}