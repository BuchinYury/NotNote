package io.buchin.notenote.features.note

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.textChanges
import io.buchin.notenote.R
import io.buchin.notenote.entitys.NoteEntry
import io.buchin.notenote.features.note.NoteIntention.AddNewNoteEntryClicked
import io.buchin.notenote.features.note.NoteIntention.DeleteNoteEntryClicked
import io.buchin.notenote.features.note.NoteIntention.NoteEntryIsDoneChanged
import io.buchin.notenote.features.note.NoteIntention.NoteEntryTextChanged
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.item_add_new_note_content.view.button_add_new_note_content
import kotlinx.android.synthetic.main.item_note_content.view.checbox_note_content_is_done
import kotlinx.android.synthetic.main.item_note_content.view.edittext_note_content
import kotlinx.android.synthetic.main.item_note_content.view.imageview_note_content_delete

class NoteContentAdapter(
    private val entries: List<NoteEntry>,
    private val viewModel: NoteViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
      when (viewType) {
        0 -> AddNewNoteEntryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_add_new_note_content, parent, false),
            viewModel
        )
        else -> NoteEntryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_note_content, parent, false),
            viewModel
        )
      }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    (holder as? NoteEntryViewHolder)
        ?.let {
          holder.bind(entries[position])
        }
  }

  override fun getItemCount() = entries.size + 1

  override fun getItemViewType(position: Int): Int =
      when (position) {
        entries.size -> 0
        else -> 1
      }

  inner class AddNewNoteEntryViewHolder(
      view: View,
      private val viewModel: NoteViewModel
  ) : RecyclerView.ViewHolder(view) {
    init {
      itemView.apply {
        button_add_new_note_content.apply {
          clicks().map { AddNewNoteEntryClicked }
              .subscribe(viewModel.intentions)
        }
      }
    }
  }

  inner class NoteEntryViewHolder(
      view: View,
      private val viewModel: NoteViewModel
  ) : RecyclerView.ViewHolder(view) {
    fun bind(noteEntry: NoteEntry) {
      itemView.apply {
        checbox_note_content_is_done?.apply {
          isChecked = noteEntry.isDone

          checkedChanges().map { NoteEntryIsDoneChanged(noteEntry.copy(isDone = it)) }
              .subscribe(viewModel.intentions)
        }

        edittext_note_content?.apply {
          val isLastNoteEntryTextFocusable = (viewModel.viewState as BehaviorSubject).let {
            it.hasValue() && it.value.isLastNoteEntryTextFocusable
          }
          if (isLastNoteEntryTextFocusable && entries.indexOf(noteEntry) == entries.lastIndex) {
            requestFocus()
          }

          setText(noteEntry.content)
          setSelection(text.length)

          textChanges().map { it.toString() }
              .map { NoteEntryTextChanged(noteEntry.copy(content = it)) }
              .subscribe(viewModel.intentions)
        }

        imageview_note_content_delete?.apply {
          clicks().map { DeleteNoteEntryClicked(noteEntry) }
              .subscribe(viewModel.intentions)
        }
      }
    }
  }
}
