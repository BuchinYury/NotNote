package io.buchin.notenote.features.notes

import android.content.Context
import io.buchin.notenote.common.Screen
import io.buchin.notenote.common.navigateTo
import io.buchin.notenote.entitys.Note
import io.buchin.notenote.features.notes.NotesIntention.AddNewNoteClicked
import io.buchin.notenote.features.notes.NotesIntention.DeleteNote
import io.buchin.notenote.features.notes.NotesIntention.DeleteNoteCanceled
import io.buchin.notenote.features.notes.NotesIntention.DeleteNoteConfirmed
import io.buchin.notenote.features.notes.NotesIntention.NoteBackgroundCheckedChanges
import io.buchin.notenote.features.notes.NotesIntention.NoteClicked
import io.buchin.notenote.features.notes.NotesIntention.SettingsItemClicked
import io.buchin.notenote.features.notes.NotesSideEffect.CancelNoteNotification
import io.buchin.notenote.features.notes.NotesSideEffect.ShowNoteNotification
import io.buchin.notenote.interactors.NoteInteractor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router

class NotesViewModel(
    initialState: NotesViewState,
    disposeBag: CompositeDisposable,
    router: Router,
    noteInteractor: NoteInteractor
) {
  val intentions: Subject<NotesIntention> = PublishSubject.create()
  val sideEffects: Subject<NotesSideEffect> = PublishSubject.create()
  val viewState: Subject<NotesViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val screenResumed = intentions.filter { it is NotesIntention.ScreenResumed }
        .map {
          { currentState: NotesViewState ->
            currentState.copy(notes = noteInteractor.getNotes())
          }
        }
    val deleteNote = intentions.filter { it is DeleteNote }
        .map { (it as DeleteNote).note }
        .map {
          { currentState: NotesViewState ->
            currentState.copy(deleteNote = Pair(true, it))
          }
        }
    val deleteNoteConfirmed = intentions.filter { it === DeleteNoteConfirmed }
        .map {
          { currentState: NotesViewState ->
            noteInteractor.removeNote(currentState.deleteNote.second as Note)
            currentState.copy(
                notes = noteInteractor.getNotes(),
                deleteNote = Pair(false, null)
            )
          }
        }
    val deleteNoteCanceled = intentions.filter { it === DeleteNoteCanceled }
        .map {
          { currentState: NotesViewState ->
            currentState.copy(
                deleteNote = Pair(false, null)
            )
          }
        }
    val noteBackgroundCheckedChanges = intentions.filter { it is NoteBackgroundCheckedChanges }
        .map { (it as NoteBackgroundCheckedChanges).note }
        .withLatestFrom(viewState) { note, viewState ->
          note to viewState.notes.find { it.id == note.id }
        }
        .filter { (note, findNote) ->
          findNote != null && note.backgroundEnable != findNote.backgroundEnable
        }
        .map { (note, _) ->
          note
        }
        .doOnNext {
          if (it.backgroundEnable) {
            sideEffects.onNext(ShowNoteNotification(it))
          } else {
            sideEffects.onNext(CancelNoteNotification(it))
          }
        }
        .map { note ->
          { currentState: NotesViewState ->
            currentState.copy(
                notes = currentState.notes
                    .map {
                      if (it.id == note.id) it.copy(backgroundEnable = note.backgroundEnable)
                      else it
                    }
                    .apply {
                      noteInteractor.saveNotes(this)
                    }
            )
          }
        }

    Observable.merge(
        listOf(
            screenResumed,
            deleteNote,
            deleteNoteConfirmed,
            deleteNoteCanceled,
            noteBackgroundCheckedChanges
        )
    )
        .scan(initialState) { currentState, stateReduser ->
          stateReduser(currentState)
        }
        .subscribe(viewState)
  }

  init {
    intentions.filter { it === SettingsItemClicked }
        .subscribe {
          //          router.navigateTo(Screen.ProfileScreen)
        }
        .addTo(disposeBag)

    intentions.filter { it is NoteClicked }
        .map { (it as NoteClicked).note }
        .subscribe {
          router.navigateTo(Screen.NoteScreen(it))
        }
        .addTo(disposeBag)

    intentions.filter { it == AddNewNoteClicked }
        .subscribe {
          router.navigateTo(Screen.NoteScreen(Note()))
        }
        .addTo(disposeBag)
  }

  init {
    sideEffects.filter { it is ShowNoteNotification }
        .map { (it as ShowNoteNotification).note }
        .subscribe {
          noteInteractor.showNoteNotification(it)
        }
        .addTo(disposeBag)

    sideEffects.filter { it is CancelNoteNotification }
        .map { (it as CancelNoteNotification).note }
        .subscribe {
          noteInteractor.cancelNoteNotification(it)
        }
        .addTo(disposeBag)
  }
}

data class NotesViewState(
    val notes: List<Note> = emptyList(),
    val deleteNote: Pair<Boolean, Note?> = Pair(false, null)
)

sealed class NotesIntention {
  object ScreenResumed : NotesIntention()
  object SettingsItemClicked : NotesIntention()
  object AddNewNoteClicked : NotesIntention()
  object DeleteNoteConfirmed : NotesIntention()
  object DeleteNoteCanceled : NotesIntention()
  class NoteClicked(val note: Note) : NotesIntention()
  class DeleteNote(val note: Note) : NotesIntention()
  class NoteBackgroundCheckedChanges(val note: Note) : NotesIntention()
}

sealed class NotesSideEffect {
  class ShowNoteNotification(val note: Note) : NotesSideEffect()
  class CancelNoteNotification(val note: Note) : NotesSideEffect()
}
