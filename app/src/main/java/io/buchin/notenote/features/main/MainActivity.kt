package io.buchin.notenote.features.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.buchin.notenote.NotNoteApp
import io.buchin.notenote.R
import io.buchin.notenote.common.MainModule
import io.buchin.notenote.common.NavigationActivity
import io.buchin.notenote.common.Screen
import io.buchin.notenote.common.navigateTo
import io.buchin.notenote.common.newRootScreen
import io.buchin.notenote.entitys.Note
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required


class MainActivity : AppCompatActivity(),
    Injects<MainModule>,
    NavigationActivity {
  val mainModule by lazy {
    MainModule(
        this,
        R.id.framelayout_main_fragment_container,
        (application as NotNoteApp).appModule
    )
  }

  // region dependency injection
  private val disposeBag by required { disposeBag }
  private val navigatorHolder by required { navigatorHolder }
  private val navigator by required { navigator }
  private val router by required { router }
  private val viewModel by required { viewModel }
  // endregion

  // region activity
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(mainModule)
    setContentView(R.layout.activity_main)
    router.newRootScreen(Screen.NotesScreen)
  }

  override fun onResumeFragments() {
    super.onResumeFragments()
    navigatorHolder.setNavigator(navigator)
  }

  override fun onResume() {
    super.onResume()
    handleIntent()
  }

  override fun onPause() {
    super.onPause()
    navigatorHolder.removeNavigator()
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // region NavigationActivity
  override fun screenBecomeVisible(screen: Screen) {
    viewModel.intentions.onNext(MainIntention.ScreenVisible(screen))
  }
  // endregion

  private fun handleIntent() {
    intent.apply {
      val note = (getParcelableExtra("note") as? Note)
      if (note != null) {
        router.navigateTo(Screen.NoteScreen(note))
      }

      putExtra("note", "")
    }
  }
}
