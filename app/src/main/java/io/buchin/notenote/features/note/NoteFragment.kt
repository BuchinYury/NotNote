package io.buchin.notenote.features.note

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.buchin.notenote.R
import io.buchin.notenote.common.BaseNavigationFragment
import io.buchin.notenote.common.NoteModule
import io.buchin.notenote.common.Screen
import io.buchin.notenote.common.mainModule
import io.buchin.notenote.entitys.Note
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_note.edittext_note_title
import kotlinx.android.synthetic.main.fragment_note.imageview_toolbar_back
import kotlinx.android.synthetic.main.fragment_note.imageview_toolbar_save_note
import kotlinx.android.synthetic.main.fragment_note.recyclerview_note_content
import kotlinx.android.synthetic.main.fragment_note.textview_toolbar_title
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required

private const val PNoteKey = "PNoteKey"

class NoteFragment : BaseNavigationFragment(), Injects<NoteModule> {
  companion object {
    fun newInstance(note: Note) = NoteFragment().apply {
      arguments = Bundle().apply {
        putParcelable(PNoteKey, note)
      }
    }
  }

  override val screen get() = Screen.NoteScreen(note)
  // region dependency injection
  private val viewModel by required { viewModel }
  private val disposeBag by required { disposeBag }
  private val noteContentAdapterProvider by required { noteContentAdapterProvider }
  // endregion

  private val note: Note
    get() = arguments?.getParcelable(PNoteKey) as Note

  // region Fragment
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(NoteModule(requireContext(), note, mainModule))
  }

  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_note, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    handleToolbar()
    handleNoteTitle()
    handleNoteContent()
  }

  override fun onResume() {
    super.onResume()
    viewModel.intentions.onNext(NoteIntention.ScreenResumed)
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // regionHandle
  private fun handleToolbar() {
    imageview_toolbar_back?.apply {
      clicks().map { NoteIntention.BackClicked }
          .subscribe(viewModel.intentions)
    }

    textview_toolbar_title?.apply {
      viewModel.viewState
          .map { it.toolbarTitle }
          .distinctUntilChanged()
          .filter { it != text.toString() }
          .subscribe {
            text = it
          }
          .addTo(disposeBag)
    }

    imageview_toolbar_save_note?.apply {
      clicks().map { NoteIntention.SaveNoteClicked }
          .subscribe(viewModel.intentions)
    }
  }

  private fun handleNoteTitle() {
    edittext_note_title?.apply {
      viewModel.viewState
          .map { it.noteTitle }
          .distinctUntilChanged()
          .filter { it != text.toString() }
          .subscribe {
            setText(it)
            setSelection(it.length)
          }
          .addTo(disposeBag)

      textChanges()
          .map { it.toString() }
          .map { NoteIntention.NoteTitleTextChanged(it) }
          .subscribe(viewModel.intentions)
    }
  }

  private fun handleNoteContent() {
    recyclerview_note_content?.apply {
      isNestedScrollingEnabled = true
      layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
      adapter = noteContentAdapterProvider(note.noteContent)

      viewModel.viewState
          .map { it.noteEntrys }
          .distinctUntilChanged { currentState, newState ->
            currentState.size == newState.size
          }
          .subscribe {
            adapter = noteContentAdapterProvider(it)
          }
          .addTo(disposeBag)
    }
  }
  // endregion
}
