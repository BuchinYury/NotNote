package io.buchin.notenote.features.note

import io.buchin.notenote.entitys.Note
import io.buchin.notenote.entitys.NoteEntry
import io.buchin.notenote.features.note.NoteIntention.AddNewNoteEntryClicked
import io.buchin.notenote.features.note.NoteIntention.BackClicked
import io.buchin.notenote.features.note.NoteIntention.DeleteNoteEntryClicked
import io.buchin.notenote.features.note.NoteIntention.NoteEntryIsDoneChanged
import io.buchin.notenote.features.note.NoteIntention.NoteEntryTextChanged
import io.buchin.notenote.features.note.NoteIntention.NoteTitleTextChanged
import io.buchin.notenote.features.note.NoteIntention.SaveNoteClicked
import io.buchin.notenote.features.note.NoteIntention.ScreenResumed
import io.buchin.notenote.interactors.NoteInteractor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router

class NoteViewModel(
    initialState: NoteViewState,
    disposeBag: CompositeDisposable,
    router: Router,
    noteInteractor: NoteInteractor,
    private val note: Note
) {
  val intentions: Subject<NoteIntention> = PublishSubject.create()
  val sideEffects: Subject<NoteSideEffect> = PublishSubject.create()
  val viewState: Subject<NoteViewState> = BehaviorSubject.createDefault(initialState)

  private val toolbarTitle
    get() = when {
      note.title.isEmpty() && note.noteContent.isEmpty() -> "Новая заметка"
      else -> "Редактирование заметки"
    }

  init {
    val screenResumed = intentions.filter { it === ScreenResumed }
        .map {
          { currentState: NoteViewState ->
            currentState
          }
        }
    val toolbarTitle = Observable.just(toolbarTitle)
        .map {
          { currentState: NoteViewState ->
            currentState.copy(
                toolbarTitle = it
            )
          }
        }
    val noteTitle = intentions.filter { it is NoteTitleTextChanged }
        .map { (it as NoteTitleTextChanged).noteTitle }
        .withLatestFrom(viewState) { newNoteTitle, viewState ->
          newNoteTitle to viewState.noteTitle
        }
        .filter { (newNoteTitle, currentNoteTitle) ->
          newNoteTitle != currentNoteTitle
        }
        .map { (newNoteTitle, _) ->
          newNoteTitle
        }
        .map {
          { currentState: NoteViewState ->
            currentState.copy(noteTitle = it)
          }
        }
    val addNewNoteEntryClicked = intentions.filter { it === AddNewNoteEntryClicked }
        .map {
          { currentState: NoteViewState ->
            currentState.copy(
                noteEntrys = currentState.noteEntrys
                    .toMutableList()
                    .let {
                      it.add(NoteEntry())
                      it
                    }
                    .toList(),
                isLastNoteEntryTextFocusable = true
            )
          }
        }
    val deleteNoteEntryClicked = intentions.filter { it is DeleteNoteEntryClicked }
        .map { (it as DeleteNoteEntryClicked).noteEntry }
        .map { noteEntry ->
          { currentState: NoteViewState ->
            currentState.copy(
                noteEntrys = currentState.noteEntrys
                    .filter { it.id != noteEntry.id }
            )
          }
        }
    val noteEntryIsDoneChanged = intentions.filter { it is NoteEntryIsDoneChanged }
        .map { (it as NoteEntryIsDoneChanged).noteEntry }
        .withLatestFrom(viewState) { noteEntry, viewState ->
          noteEntry to viewState.noteEntrys.find { it.id == noteEntry.id }
        }
        .filter { (noteEntry, findNoteEntry) ->
          findNoteEntry != null && noteEntry.isDone != findNoteEntry.isDone
        }
        .map { (noteEntry, _) ->
          noteEntry
        }
        .map { noteEntry ->
          { currentState: NoteViewState ->
            currentState.copy(
                noteEntrys = currentState.noteEntrys
                    .map {
                      if (it.id == noteEntry.id) it.copy(isDone = noteEntry.isDone)
                      else it
                    }
            )
          }
        }
    val noteEntryTextChanged = intentions.filter { it is NoteEntryTextChanged }
        .map { (it as NoteEntryTextChanged).noteEntry }
        .withLatestFrom(viewState) { noteEntry, viewState ->
          noteEntry to viewState.noteEntrys.find { it.id == noteEntry.id }
        }
        .filter { (noteEntry, findNoteEntry) ->
          findNoteEntry != null && noteEntry.content != findNoteEntry.content
        }
        .map { (noteEntry, _) ->
          noteEntry
        }
        .map { noteEntry ->
          { currentState: NoteViewState ->
            currentState.copy(
                noteEntrys = currentState.noteEntrys
                    .map {
                      if (it.id == noteEntry.id) it.copy(content = noteEntry.content)
                      else it
                    }
            )
          }
        }

    Observable.merge(
        listOf(
            screenResumed,
            toolbarTitle,
            noteTitle,
            addNewNoteEntryClicked,
            deleteNoteEntryClicked,
            noteEntryIsDoneChanged,
            noteEntryTextChanged
        )
    )
        .scan(initialState) { currentState, stateReducer ->
          stateReducer(currentState)
        }
        .subscribe(viewState)
  }

  init {
    intentions.filter { it === BackClicked }
        .subscribe { router.exit() }
        .addTo(disposeBag)

    intentions.filter { it === SaveNoteClicked }
        .withLatestFrom(viewState) { _, viewState ->
          note.copy(
              title = viewState.noteTitle,
              noteContent = viewState.noteEntrys
          )
        }
        .subscribe {
          noteInteractor.saveNote(it)
          router.exit()
        }
        .addTo(disposeBag)
  }
}

data class NoteViewState(
    val toolbarTitle: String = "",
    val noteTitle: String = "",
    val noteEntrys: List<NoteEntry> = emptyList(),
    val isLastNoteEntryTextFocusable: Boolean = false
)

sealed class NoteIntention {
  object ScreenResumed : NoteIntention()
  object BackClicked : NoteIntention()
  object SaveNoteClicked : NoteIntention()
  object AddNewNoteEntryClicked : NoteIntention()
  class NoteTitleTextChanged(val noteTitle: String) : NoteIntention()
  class DeleteNoteEntryClicked(val noteEntry: NoteEntry) : NoteIntention()
  class NoteEntryIsDoneChanged(val noteEntry: NoteEntry) : NoteIntention()
  class NoteEntryTextChanged(val noteEntry: NoteEntry) : NoteIntention()
}

sealed class NoteSideEffect
