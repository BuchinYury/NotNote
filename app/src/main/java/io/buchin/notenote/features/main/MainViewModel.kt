package io.buchin.notenote.features.main

import io.buchin.notenote.common.Screen
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class MainViewModel(
    initialState: MainViewState
) {
  val intentions: Subject<MainIntention> = PublishSubject.create()
  val sideEffects: Subject<MainSideEffect> = PublishSubject.create()
  val viewState: Subject<MainViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val screenVisible = intentions.filter { it is MainIntention.ScreenVisible }
        .map { (it as MainIntention.ScreenVisible).screen }
        .map { screen ->
          { currentState: MainViewState ->
            currentState.copy(screen = screen)
          }
        }

    Observable.merge(listOf(screenVisible))
        .scan(initialState) { currentState, stateReduser ->
          stateReduser(currentState)
        }
        .subscribe(viewState)
  }

}

data class MainViewState(
    val screen: Screen = Screen.NotesScreen
)

sealed class MainIntention {
  class ScreenVisible(val screen: Screen) : MainIntention()
}

sealed class MainSideEffect
