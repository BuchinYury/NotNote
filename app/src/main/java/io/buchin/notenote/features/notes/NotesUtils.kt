package io.buchin.notenote.features.notes

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.longClicks
import com.jakewharton.rxbinding2.widget.checkedChanges
import io.buchin.notenote.R
import io.buchin.notenote.entitys.Note
import io.buchin.notenote.features.notes.NotesIntention.AddNewNoteClicked
import io.buchin.notenote.features.notes.NotesIntention.NoteBackgroundCheckedChanges
import io.buchin.notenote.features.notes.NotesIntention.DeleteNote
import io.buchin.notenote.features.notes.NotesIntention.NoteClicked
import kotlinx.android.synthetic.main.item_add_new_note.view.button_add_new_note
import kotlinx.android.synthetic.main.item_note.view.switchcompat_note_background
import kotlinx.android.synthetic.main.item_note.view.textview_note_title

class NotesAdapter(
    var items: List<Note>,
    private val viewModel: NotesViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
      when (viewType) {
        0 -> AddNewNoteViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_add_new_note, parent, false),
            viewModel
        )
        else -> NoteViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_note, parent, false),
            viewModel
        )
      }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
      when (holder.itemViewType) {
        0 -> (holder as AddNewNoteViewHolder).let {
          when (position) {
            0 -> holder.bindFirst()
            itemCount - 1 -> holder.bindLast()
            else -> holder.bind()
          }
        }
        else -> (holder as NoteViewHolder).let {
          position.let {
            when (it) {
              0 -> holder.bindFirst(items[it])
              else -> holder.bind(items[it])
            }
          }
        }
      }

  override fun getItemCount() = items.size + 1

  override fun getItemViewType(position: Int): Int =
      when (position) {
        items.size -> 0
        else -> 1
      }

  inner class AddNewNoteViewHolder(
      private val view: View,
      private val viewModel: NotesViewModel
  ) : RecyclerView.ViewHolder(view) {
    private val noteMargin get() = view.resources.getDimension(R.dimen.item_notes_margin).toInt()

    fun bind() {
      itemView.apply {
        button_add_new_note.setOnClickListener {
          it.isClickable = false
          viewModel.intentions.onNext(AddNewNoteClicked)
          it.isClickable = true
        }
      }
    }

    fun bindFirst() {
      bind()
      (itemView.layoutParams as? ViewGroup.MarginLayoutParams)?.topMargin = noteMargin
    }

    fun bindLast() {
      bind()
      (itemView.layoutParams as? ViewGroup.MarginLayoutParams)?.bottomMargin = noteMargin
    }
  }

  inner class NoteViewHolder(
      private val view: View,
      private val viewModel: NotesViewModel
  ) : RecyclerView.ViewHolder(view) {
    private val noteMargin get() = view.resources.getDimension(R.dimen.item_notes_margin).toInt()

    fun bind(note: Note) {
      itemView.apply {
        clicks().map { NoteClicked(note) }
            .subscribe(viewModel.intentions)

        longClicks().map { DeleteNote(note) }
            .subscribe(viewModel.intentions)

        textview_note_title.apply {
          if (text != note.title)
            text = note.title
        }

        switchcompat_note_background.apply {
          if (isChecked != note.backgroundEnable)
            isChecked = note.backgroundEnable

          checkedChanges()
              .map { NoteBackgroundCheckedChanges(note.copy(backgroundEnable = it)) }
              .subscribe(viewModel.intentions)
        }
      }
    }

    fun bindFirst(note: Note) {
      bind(note)
      (itemView.layoutParams as? ViewGroup.MarginLayoutParams)?.topMargin = noteMargin
    }

    fun bindLast(note: Note) {
      bind(note)
      (itemView.layoutParams as? ViewGroup.MarginLayoutParams)?.bottomMargin = noteMargin
    }
  }
}