package io.buchin.notenote.features.notes

import android.app.NotificationManager
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jakewharton.rxbinding2.support.v7.widget.itemClicks
import io.buchin.notenote.R
import io.buchin.notenote.common.BaseNavigationFragment
import io.buchin.notenote.common.NotesModule
import io.buchin.notenote.common.Screen
import io.buchin.notenote.common.hideSoftKeyboard
import io.buchin.notenote.common.mainModule
import io.buchin.notenote.entitys.Note
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_notes.recyclerview_notes
import kotlinx.android.synthetic.main.fragment_notes.toolbar_notes
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required


class NotesFragment : BaseNavigationFragment(), Injects<NotesModule> {
  override val screen get() = Screen.NotesScreen
  // region dependency injection
  private val viewModel by required { viewModel }
  private val disposeBag by required { disposeBag }
  private val notesAdapterProvider by required { notesAdapterProvider }
  private val deleteNoteAlertDialogProvider by required { deleteNoteAlertDialogProvider }
  private val onPauseDisposeBagProvider by required { onPauseDisposeBagProvider }
  // endregion

  private lateinit var onPauseDisposeBag: CompositeDisposable

  // region Fragment
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(NotesModule(requireContext(), mainModule))
  }

  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_notes, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    handleToolbar()
    handleNotes()
  }

  override fun onResume() {
    super.onResume()
    onPauseDisposeBag = onPauseDisposeBagProvider()
    handleDeleteNote()
    viewModel.intentions.onNext(NotesIntention.ScreenResumed)
    view?.hideSoftKeyboard()
  }

  override fun onPause() {
    super.onPause()
    onPauseDisposeBag.dispose()
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // regionHandle
  private fun handleToolbar() {
//    toolbar_notes.inflateMenu(R.menu.settings_menu)

    toolbar_notes.itemClicks()
        .subscribe { menuItem ->
          when (menuItem.itemId) {
            R.id.item_settings -> {
              Toast.makeText(context, "Экран настроек", Toast.LENGTH_SHORT).show()
              viewModel.intentions.onNext(NotesIntention.SettingsItemClicked)
            }
            else -> {
            }
          }
        }
        .addTo(disposeBag)
  }

  private fun handleNotes() {
    recyclerview_notes?.apply {
      setHasFixedSize(true)
      isNestedScrollingEnabled = false
      layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
      adapter = notesAdapterProvider(emptyList())

      viewModel.viewState
          .map { it.notes }
          .distinctUntilChanged { currentNotes, newNotes ->
            currentNotes.toTypedArray().contentDeepEquals(newNotes.toTypedArray())
          }
          .subscribe {
            adapter = notesAdapterProvider(it)
          }
          .addTo(disposeBag)
    }
  }

  private fun handleDeleteNote() {
    viewModel.viewState
        .map { it.deleteNote }
        .distinctUntilChanged()
        .filter { (deleteNoteAlertDialogVisible, noteToDelete) ->
          deleteNoteAlertDialogVisible && noteToDelete != null
        }
        .map { (_, noteToDelete) ->
          noteToDelete as Note
        }
        .subscribe {
          deleteNoteAlertDialogProvider(it).show()
        }
        .addTo(onPauseDisposeBag)
  }
  // endregion
}
