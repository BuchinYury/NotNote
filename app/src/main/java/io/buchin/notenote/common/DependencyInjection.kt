package io.buchin.notenote.common

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.IdRes
import android.support.v4.app.FragmentActivity
import io.buchin.notenote.BuildConfig
import io.buchin.notenote.NotNoteApp
import io.buchin.notenote.entitys.Note
import io.buchin.notenote.entitys.NoteEntry
import io.buchin.notenote.features.main.MainViewModel
import io.buchin.notenote.features.main.MainViewState
import io.buchin.notenote.features.note.NoteContentAdapter
import io.buchin.notenote.features.note.NoteViewModel
import io.buchin.notenote.features.note.NoteViewState
import io.buchin.notenote.features.notes.NotesAdapter
import io.buchin.notenote.features.notes.NotesIntention.DeleteNoteCanceled
import io.buchin.notenote.features.notes.NotesIntention.DeleteNoteConfirmed
import io.buchin.notenote.features.notes.NotesViewModel
import io.buchin.notenote.features.notes.NotesViewState
import io.buchin.notenote.interactors.NoteInteractor
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

interface AppModuleDeps {
  val context: Context
  val sharedPreferences: SharedPreferences
  val navigatorHolder: NavigatorHolder
  val router: Router
}

class AppModule(
    override val context: NotNoteApp
) : AppModuleDeps {
  private val cicerone: Cicerone<Router> = Cicerone.create()
  override val navigatorHolder: NavigatorHolder = cicerone.navigatorHolder
  override val router: Router = cicerone.router
  override val sharedPreferences: SharedPreferences
    get() = context.getSharedPreferences(BuildConfig.prefsName, Context.MODE_PRIVATE)
}

interface MainModuleDeps {
  val disposeBag: CompositeDisposable
  val navigatorHolder: NavigatorHolder
  val navigator: MainActivityNavigator
  val router: Router
  val viewModel: MainViewModel
}

class MainModule(
    val activity: FragmentActivity,
    @IdRes fragmentContainerId: Int,
    val appModule: AppModule
) : MainModuleDeps {
  override val disposeBag = CompositeDisposable()
  override val navigatorHolder = appModule.navigatorHolder
  override val navigator = MainActivityNavigator(activity, fragmentContainerId)
  override val router = appModule.router
  override val viewModel = MainViewModel(
      MainViewState()
  )
}

interface NotesModuleDeps {
  val disposeBag: CompositeDisposable
  val noteInteractor: NoteInteractor
  val router: Router
  val viewModel: NotesViewModel
  val notesAdapterProvider: (List<Note>) -> NotesAdapter
  val onPauseDisposeBagProvider: () -> CompositeDisposable
  val deleteNoteAlertDialogProvider: (Note) -> AlertDialog
}

class NotesModule(
    context: Context,
    mainModule: MainModule
) : NotesModuleDeps {
  override val disposeBag = CompositeDisposable()
  override val noteInteractor = NoteInteractor(
      context,
      mainModule.appModule.sharedPreferences
  )
  override val router = mainModule.router
  override val viewModel = NotesViewModel(
      NotesViewState(),
      disposeBag,
      router,
      noteInteractor
  )
  override val notesAdapterProvider = { notes: List<Note> ->
    NotesAdapter(notes, viewModel)
  }
  override val onPauseDisposeBagProvider = { CompositeDisposable() }
  override val deleteNoteAlertDialogProvider = { note: Note ->
    AlertDialog.Builder(mainModule.activity)
        .setMessage("Удалить заметку ${note.title}?")
        .setPositiveButton("Да") { dialog, _ ->
          viewModel.intentions.onNext(DeleteNoteConfirmed)
          dialog.dismiss()
        }
        .setNegativeButton("Отмена") { dialog, _ ->
          viewModel.intentions.onNext(DeleteNoteCanceled)
          dialog.dismiss()
        }
        .create()
  }
}

interface NoteModuleDeps {
  val disposeBag: CompositeDisposable
  val noteInteractor: NoteInteractor
  val router: Router
  val viewModel: NoteViewModel
}

class NoteModule(
    context: Context,
    note: Note,
    mainModule: MainModule
) : NoteModuleDeps {
  override val noteInteractor = NoteInteractor(
      context,
      mainModule.appModule.sharedPreferences
  )
  override val disposeBag = CompositeDisposable()
  override val router = mainModule.router
  override val viewModel = NoteViewModel(
      NoteViewState(
          noteTitle = note.title,
          noteEntrys = note.noteContent
      ),
      disposeBag,
      router,
      noteInteractor,
      note
  )
  val noteContentAdapterProvider = { noteEntrys: List<NoteEntry> ->
    NoteContentAdapter(noteEntrys, viewModel)
  }
}
