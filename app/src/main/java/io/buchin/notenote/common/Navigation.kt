package io.buchin.notenote.common

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import io.buchin.notenote.entitys.Note
import io.buchin.notenote.features.note.NoteFragment
import io.buchin.notenote.features.notes.NotesFragment
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator

fun Router.navigateTo(screen: Screen) = navigateTo(screen::class.java.simpleName, screen)

fun Router.newRootScreen(screen: Screen) = newRootScreen(screen::class.java.simpleName, screen)

sealed class Screen {
  object NotesScreen : Screen()
  object ProfileScreen : Screen()
  class NoteScreen(val note: Note) : Screen()
}

class MainActivityNavigator(activity: FragmentActivity, @IdRes containerId: Int)
  : SupportAppNavigator(activity, containerId) {

  override fun createFragment(screenKey: String, data: Any?): Fragment = when (data) {
    Screen.NotesScreen -> NotesFragment()
    is Screen.NoteScreen -> NoteFragment.newInstance(data.note)
    else -> throw IllegalArgumentException("Неизвестный экран: $screenKey")
  }

  override fun createActivityIntent(context: Context, screenKey: String, data: Any?) = null
}

interface NavigationActivity {
  fun screenBecomeVisible(screen: Screen)
}

interface NavigationFragment {
  val screen: Screen
  fun becomeVisible()
}

abstract class BaseNavigationFragment : Fragment(), NavigationFragment {
  override fun becomeVisible() {
    (activity as? NavigationActivity)?.screenBecomeVisible(screen)
  }

  override fun onResume() {
    super.onResume()
    becomeVisible()
  }
}
