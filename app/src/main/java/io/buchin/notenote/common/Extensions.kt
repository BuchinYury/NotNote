package io.buchin.notenote.common

import android.app.Activity
import android.os.IBinder
import android.support.v4.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import io.buchin.notenote.features.main.MainActivity

val Fragment.mainModule
  get() = (activity as MainActivity).mainModule

fun Activity.hideSoftKeyboard(windowToken: IBinder) {
  (this.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
      ?.hideSoftInputFromWindow(windowToken, 0)
}

fun View.hideSoftKeyboard() {
  (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
      ?.hideSoftInputFromWindow(this.windowToken, 0)
}
